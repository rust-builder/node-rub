//! Rust Builder for [Node](https://github.com/rust-lang/node).
//!
//! This crate is intended for use with
//! [rub](https://github.com/rust-builder/rub).
//!
//! If you don't have `rub` installed, visit https://github.com/rust-builder/rub
//! for installation instructions.
//!
//! # Rub Options
//! <pre>
//! $ rub node --help
//! node - Rust Builder
//!
//! Usage:
//!     rub node [options] [&lt;lifecycle&gt;...]
//!     rub node (-h | --help)
//!     rub node --version
//!
//! Options:
//!     -d --dir &lt;dir&gt;        Set the projects directory.
//!     -b --branch &lt;branch&gt;  Set the build branch. [default: master]
//!     -t --enable-test      Enable tests.
//!     -p --prefix &lt;prefix&gt;  Set the installation prefix.
//!     -u --url &lt;url&gt;        Set the SCM URL.
//!     -h --help             Show this usage.
//!     --force-configure     Force the configure lifecycle to run.
//!     --disable-cares       Disable c-ares as a shared library (v0.9 & v0.10 only).
//!     --disable-shared-v8   Disable shared v8.
//!     --disable-optimize    Disable compile-time optimization.
//!     --version             Show node-rub version.
//! </pre>
//!
//! # Examples
//! ```rust
//! # extern crate buildable; extern crate node_rub; fn main() {
//! use buildable::Buildable;
//! use node_rub::NodeRub;
//!
//! // To run lifecycle methods outside of rub...
//! let mut cr = NodeRub::new();
//! let b = Buildable::new(&mut cr, &vec!["rub".to_string(),
//!                                       "node".to_string(),
//!                                       "--version".to_string()]);
//! assert_eq!(Ok(0), b.version());
//! # }
//! ```
#![experimental]
#![allow(unstable)]
extern crate buildable;
extern crate commandext;
extern crate docopt;
extern crate regex;
extern crate "rustc-serialize" as rustc_serialize;
extern crate scm;
extern crate utils;

use buildable::{Buildable,BuildConfig,LifeCycle};
use commandext::{CommandExt,to_res};
use docopt::Docopt;
use regex::Regex;
use scm::git::GitCommand;
use std::default::Default;
use std::io::{BufferedReader,File};
use std::io::fs;
use std::io::fs::PathExtensions;
use utils::usable_cores;
use utils::empty::to_opt;

static USAGE: &'static str = "node - Rust Builder

Usage:
    rub node [options] [<lifecycle>...]
    rub node (-h | --help)
    rub node --version

Options:
    -d --dir <dir>        Set the projects directory.
    -b --branch <branch>  Set the build branch. [default: master]
    -t --enable-test      Enable tests.
    -p --prefix <prefix>  Set the installation prefix.
    -u --url <url>        Set the SCM URL.
    -h --help             Show this usage.
    --force-configure     Force the configure lifecycle to run.
    --disable-cares       Disable c-ares as a shared library (v0.9 & v0.10 only).
    --disable-shared-v8   Disable shared v8 (v0.11 & v0.12 only).
    --version             Show node-rub version.";

include!(concat!(env!("OUT_DIR"), "/version.rs"));

#[derive(RustcDecodable)]
struct Args {
    flag_dir: String,
    flag_branch: String,
    flag_enable_test: bool,
    flag_prefix: String,
    flag_url: String,
    flag_force_configure: bool,
    flag_disable_cares: bool,
    flag_disable_shared_v8: bool,
    flag_version: bool,
    flag_help: bool,
    arg_lifecycle: Vec<String>,
}

fn is_vx(x: &str, branch: &str) -> bool {
    let mut blah = String::from_str(r"^v0\.");
    blah.push_str(x);
    blah.push_str(r"(\.\d{1,2}-release)?$");

    let re = Regex::new(blah.as_slice()).unwrap();
    re.is_match(branch)
}

fn is_v10(branch: &str) -> bool {
    is_vx("10", branch)
}

fn is_v9(branch: &str) -> bool {
    is_vx("9", branch)
}

fn is_v8(branch: &str) -> bool {
    is_vx("8", branch)
}

#[cfg(target_os = "linux")]
fn os_clean(wd: &Path) -> Result<u8,u8> {
    let mut cmd = CommandExt::new("sudo");
    cmd.wd(wd);
    cmd.header(true);
    cmd.args(&["make", "clean"]);
    cmd.exec(to_res())
}

#[cfg(not(target_os = "linux"))]
fn os_clean(wd: &Path) -> Result<u8,u8> {
    let mut cmd = CommandExt::new("make");
    cmd.wd(wd);
    cmd.header(true);
    cmd.args(&["clean"]);
    cmd.exec(to_res())
}

#[cfg(target_os = "linux")]
fn os_install(wd: &Path, prefix: &str) -> Result<u8,u8> {
    println!("Prefix: {}", prefix);
    let ip = Path::new(prefix);

    if !ip.exists() {
        let mut mcmd = String::from_str("mkdir -m 775 -p ");
        mcmd.push_str(ip.as_str().unwrap());
        let mut cmd = CommandExt::new("sudo");
        cmd.header(true);
        cmd.args(&["sg", "node", "-c", mcmd.as_slice()]);
        cmd.exec(to_res())
    } else {
        Ok(0)
    }.and({
        let mut permdcmd = String::from_str("find ");
        permdcmd.push_str(ip.as_str().unwrap());
        permdcmd.push_str(" -perm 755 -exec chmod 775 {} \\;");

        let mut permfcmd = String::from_str("find ");
        permfcmd.push_str(ip.as_str().unwrap());
        permfcmd.push_str(" -perm 644 -exec chmod 664 {} \\;");

        let mut cmd = CommandExt::new("sudo");
        cmd.wd(wd);
        cmd.header(true);
        cmd.args(&["sg", "node", "-c", "make install"]);
        cmd.exec(to_res()).and({
            let mut pdc = CommandExt::new("sudo");
            pdc.header(true);
            pdc.args(&["sg", "node", "-c", permdcmd.as_slice()]);
            pdc.exec(to_res())
        }).and({
            let mut pfc = CommandExt::new("sudo");
            pfc.header(true);
            pfc.args(&["sg", "node", "-c", permfcmd.as_slice()]);
            pfc.exec(to_res())
        })
    })
}

#[cfg(not(target_os = "linux"))]
fn os_install(wd: &Path, _: &str) -> Result<u8,u8> {
    let mut cmd = CommandExt::new("make");
    cmd.wd(wd);
    cmd.header(true);
    cmd.args(&["install"]);
    cmd.exec(to_res())
}

/// Node specific configuration for the `Buildable` lifecycle methods.
#[experimental]
#[derive(Clone,Default)]
pub struct NodeRub {
    config: BuildConfig,
    prefix: String,
    url: String,
    force_config: bool,
    disable_cares: bool,
    disable_v8: bool,
}

impl NodeRub {
    /// Create a new default NodeRub.
    pub fn new() -> NodeRub {
        Default::default()
    }
}

impl Buildable for NodeRub {
    /// Update the NodeRub struct after parsing the given args vector.
    ///
    /// Normally, the args vector would be supplied from the command line, but
    /// they can be supplied as in the example below as well.
    ///
    /// # Example
    /// ```rust
    /// # extern crate buildable; extern crate node_rub; fn main() {
    /// use buildable::Buildable;
    /// use node_rub::NodeRub;
    ///
    /// // To run lifecycle methods outside of rub...
    /// let mut cr = NodeRub::new();
    /// let b = Buildable::new(&mut cr, &vec!["rub".to_string(),
    ///                                       "node".to_string()]);
    /// assert_eq!(Ok(0), b.version());
    /// # }
    fn new(&mut self, args: &Vec<String>) -> &mut NodeRub {
        let dargs: Args = Docopt::new(USAGE)
            .and_then(|d| Ok(d.help(false)))
            .and_then(|d| d.argv(args.clone().into_iter()).decode())
            .unwrap_or_else( |e| e.exit());
        self.prefix = dargs.flag_prefix;
        self.url = dargs.flag_url;
        self.force_config = dargs.flag_force_configure;
        self.disable_cares = dargs.flag_disable_cares;
        self.disable_v8 = dargs.flag_disable_shared_v8;

        if dargs.flag_version {
            let mut cfg = BuildConfig::new();
            cfg.lifecycle(vec!["version"]);
            self.config = cfg;
        } else if dargs.flag_help {
            let mut cfg = BuildConfig::new();
            cfg.lifecycle(vec!["help"]);
            self.config = cfg;
        } else {
            let mut cfg = BuildConfig::new();

            if to_opt(dargs.flag_dir.as_slice()).is_some() {
                cfg.dir(Path::new(dargs.flag_dir.as_slice()));
            }
            cfg.project("node");
            if to_opt(dargs.flag_branch.as_slice()).is_some() {
                cfg.branch(dargs.flag_branch.as_slice());
            }
            cfg.test(dargs.flag_enable_test);

            let lc = dargs.arg_lifecycle;
            if to_opt(lc.clone()).is_some() {
                let mut mylc = Vec::new();
                for lc in lc.iter() {
                    mylc.push(lc.as_slice());
                }
                cfg.lifecycle(mylc);
            }
            self.config = cfg;
        }
        self
    }

    /// Get the `BuildConfig` associated with the `NodeRub`.
    ///
    /// # Example
    /// ```rust
    /// # extern crate buildable; extern crate node_rub; fn main() {
    /// use buildable::Buildable;
    /// use node_rub::NodeRub;
    ///
    /// // To run lifecycle methods outside of rub...
    /// let mut cr = NodeRub::new();
    /// let b = Buildable::new(&mut cr, &vec!["rub".to_string(),
    ///                                       "node".to_string()]);
    /// let bc = b.get_bc();
    /// assert_eq!("node", bc.get_project());
    /// # }
    fn get_bc(&self) -> &BuildConfig {
        &self.config
    }

    /// No lifecycle reorder is necessary for node.
    fn reorder<'a>(&self, lc: &'a mut Vec<LifeCycle>) -> &'a mut Vec<LifeCycle> {
        lc
    }

    /// Check for node dependencies.
    ///
    /// TODO: Implement
    fn chkdeps(&self) -> Result<u8,u8> {
        Ok(0)
    }

    /// Peform the git operations necessary to get the project directory ready
    /// for the rest of the build lifecycle operations.
    ///
    /// # Notes
    /// * If the project directory doesn't exist, `node` will be cloned from
    /// github automatically.  You can adjust where is is cloned from by using
    /// the `--url` flag at the command line. The default is
    /// git@github.com:joyent/node.git.
    /// * If the project does exist, the requested branch is updated via
    /// `update_branch` to prepare for the rest of the build cycle.
    fn scm(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let base = Path::new(cfg.get_dir());
        let u = if self.url.is_empty() {
            "git@github.com:joyent/node.git"
        } else {
            self.url.as_slice()
        };

        let mut cmd = GitCommand::new();
        cmd.wd(base.clone());
        cmd.verbose(true);

        if !base.join(cfg.get_project()).exists() {
            cmd.clone(Some(vec!["--recursive", u]), to_res())
        } else {
            Ok(0)
        }.and({
            cmd.wd(base.join(cfg.get_project()));
            cmd.update_branch(self.config.get_branch())
        })
    }

    /// Run `make clean` in the project directory.
    ///
    /// # Notes
    /// * This will fail if `configure` hasn't be run at least once, because
    /// no `Makefile` will exist.
    fn clean(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        os_clean(&Path::new(cfg.get_dir()).join(cfg.get_project()))
    }

    /// Run `configure` in the project directory.
    ///
    /// # Examples
    /// <pre>
    /// rub node --force-configure
    /// rub node --prefix=/usr/local --disable-optimize
    /// rub node -b auto
    /// </pre>
    ///
    /// # Notes
    /// * If a rustc hasn't been installed locally to the project directory or
    /// `--force-config` was supplied then `.travis.install.deps.sh` will be run
    /// to do so.
    /// * If a `Makefile` doesn't exist or `--force-config` was supplied then
    /// configure will run.  Otherwise, it will be skipped.
    /// * The default `configure` command is setup as:
    /// ``
    /// configure --prefix=/opt/node-<branch> --enable-optimize --local-rust-root=`pwd`/rustc
    /// ``
    /// * You can adjust the `prefix` by using the `--prefix <x>` flag or the
    /// `--branch X` flag.
    /// * You can remove the `--enable-optimize` config flag by using the
    /// `--disable-optimize` flag.
    fn configure(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let wd = Path::new(cfg.get_dir()).join(cfg.get_project());

        if !wd.join("Makefile").exists() || wd.join("dirty").exists() || self.force_config {
            fs::unlink(&Path::new(wd.join("dirty"))).unwrap_or_else(|why| {
                println!("{}", why);
            });
            let mut cmd = CommandExt::new("configure");
            cmd.wd(&wd);
            cmd.header(true);

            let mut fullp = String::from_str("--prefix=");

            if self.prefix.is_empty() {
                fullp.push_str("/opt/node-");
                fullp.push_str(self.config.get_branch());
            } else {
                fullp.push_str(self.prefix.as_slice());
            }
            cmd.arg(fullp.as_slice());
            cmd.arg("--shared-zlib");
            cmd.arg("--shared-openssl");

            let branch = cfg.get_branch();

            if !self.disable_cares && (is_v9(branch) || is_v10(branch)) {
                cmd.arg("--shared-cares");
            }

            if !is_v10(branch) && !is_v8(branch) && !self.disable_v8 {
                cmd.arg("--shared-v8");

                let cpath = wd.join("deps").join("v8").join("ChangeLog");
                let changelog = File::open(&cpath);
                let mut reader = BufferedReader::new(changelog);

                let v8_version = match reader.read_line() {
                    Ok(v)  => v,
                    Err(_) => panic!("Unable to find v8 version"),
                };

                let mut ver_vec: Vec<&str> = v8_version.trim().split_str(" ")
                    .collect();
                let v8_ver = ver_vec.pop().unwrap();
                let mut inc = String::from_str("--shared-v8-includes=");
                let mut lib = String::from_str("--shared-v8-libpath=");
                let v8dir = Path::new(env!("HOME")).join("lib").join("v8")
                    .join(v8_ver);
                let incdir = v8dir.join("include");
                let libdir = v8dir.join("lib");

                inc.push_str(incdir.as_str().unwrap());
                lib.push_str(libdir.as_str().unwrap());

                cmd.arg(inc.as_slice());
                cmd.arg(lib.as_slice());
            }

            cmd.exec(to_res())
        } else {
            Ok(0)
        }
    }

    /// Run 'make' in the project directory.
    ///
    /// Runs `make -j<X>` where X is the number of usable cores (nproc - 1).
    fn make(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let mut jobs = String::from_str("-j");
        jobs.push_str(usable_cores().to_string().as_slice());
        let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
        let mut cmd = CommandExt::new("make");
        cmd.wd(&wd);
        cmd.arg(jobs.as_slice());
        cmd.header(true);

        if cfg!(windows) {
            cmd.env("CC","gcc");
        }

        cmd.exec(to_res())
    }

    /// Run 'make test' in the project directory.
    ///
    /// Runs `make test`.
    fn test(&self) -> Result<u8,u8> {
        if self.config.get_test() {
            let cfg = &self.config;
            let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
            let mut cmd = CommandExt::new("make");
            cmd.wd(&wd);
            cmd.args(&["test"]);
            cmd.header(true);
            cmd.exec(to_res())
        } else {
            Ok(0)
        }
    }

    /// Run 'make install' or 'sudo make install' in the project directory.
    ///
    /// Runs `make install`.
    fn install(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let mut prefix = String::from_str("/opt/node-");
        let pre = if self.prefix.is_empty() {
            prefix.push_str(self.config.get_branch());
            prefix.as_slice()
        } else {
            self.prefix.as_slice()
        };

        os_install(&Path::new(cfg.get_dir()).join(cfg.get_project()), pre)
    }

    /// Not implemented in this crate.
    fn cleanup(&self) -> Result<u8,u8> {
        Ok(0)
    }

    /// Show the docopt USAGE string on stdout.
    fn help(&self) -> Result<u8,u8> {
        println!("{}", USAGE);
        Ok(0)
    }

    /// Show the crate version on stdout.
    fn version(&self) -> Result<u8,u8> {
        println!("{} {} node-rub {}", now(), sha(), branch());
        Ok(0)
    }
}

#[cfg(test)]
mod test {
    use buildable::{Buildable,BuildConfig};
    use super::NodeRub;
    use super::{is_vx,is_v10,is_v9,is_v8};

    #[test]
    fn test_is_vx() {
        assert!(is_vx("5","v0.5"));
        assert!(is_vx("5","v0.5.1-release"));
        assert!(is_vx("5","v0.5.10-release"));

        assert!(!is_vx("4","v0.5"));
        assert!(!is_vx("5","v0.5-release"));
        assert!(!is_vx("5","v0.5.100-release"));
        assert!(!is_vx("5","v0.5xyz"));
        assert!(!is_vx("5","v0.5-blah?"));
    }

    #[test]
    fn test_is_v10() {
        assert!(is_v10("v0.10"));
        assert!(is_v10("v0.10.0-release"));
        assert!(is_v10("v0.10.10-release"));

        assert!(!is_v10("v0.4"));
        assert!(!is_v10("v0.10-release"));
        assert!(!is_v10("v0.10.100-release"));
        assert!(!is_v10("v0.10xyz"));
        assert!(!is_v10("v0.10-blah?"));
    }

    #[test]
    fn test_is_v9() {
        assert!(is_v9("v0.9"));
        assert!(is_v9("v0.9.0-release"));
        assert!(is_v9("v0.9.10-release"));

        assert!(!is_v9("v0.4"));
        assert!(!is_v9("v0.9-release"));
        assert!(!is_v9("v0.9.100-release"));
        assert!(!is_v9("v0.9xyz"));
        assert!(!is_v9("v0.9-blah?"));
    }

    #[test]
    fn test_is_v8() {
        assert!(is_v8("v0.8"));
        assert!(is_v8("v0.8.0-release"));
        assert!(is_v8("v0.8.10-release"));

        assert!(!is_v8("v0.4"));
        assert!(!is_v8("v0.8-release"));
        assert!(!is_v8("v0.8.100-release"));
        assert!(!is_v8("v0.8xyz"));
        assert!(!is_v8("v0.8-blah?"));
    }

    fn check_cr(cr: &NodeRub) {
        assert_eq!(cr.prefix, "");
        assert_eq!(cr.url, "");
        assert!(!cr.force_config);
        assert!(!cr.disable_cares);
    }

    fn check_bc(bc: &BuildConfig, lc: &Vec<&str>) {
        let tdir = Path::new(env!("HOME").to_string()).join("projects");
        assert_eq!(bc.get_lifecycle(), lc);
        assert_eq!(bc.get_dir().as_str().unwrap(), tdir.as_str().unwrap());
        assert_eq!(bc.get_project(), "node");
        assert_eq!(bc.get_branch(), "master");
        assert!(!bc.get_test());
    }

    #[test]
    fn test_new() {
        let cr = NodeRub::new();
        check_cr(&cr);
    }

    #[test]
    fn test_version() {
        let args = vec!["rub".to_string(),
                        "node".to_string(),
                        "--version".to_string()];
        let mut cr = NodeRub::new();
        check_cr(&cr);
        let b = Buildable::new(&mut cr, &args);
        let bc = b.get_bc();
        assert_eq!(bc.get_lifecycle(), &vec!["version"]);
        assert_eq!(b.version(), Ok(0))
    }

    #[test]
    fn test_help() {
        let args = vec!["rub".to_string(),
                        "node".to_string(),
                        "-h".to_string()];
        let mut cr = NodeRub::new();
        check_cr(&cr);
        let b = Buildable::new(&mut cr, &args);
        let bc = b.get_bc();
        assert_eq!(bc.get_lifecycle(), &vec!["help"]);
        assert_eq!(b.help(), Ok(0))
    }

    #[test]
    fn test_base() {
        let args = vec!["rub".to_string(),
                        "node".to_string()];
        let mut cr = NodeRub::new();
        check_cr(&cr);
        let b = Buildable::new(&mut cr, &args);
        let bc = b.get_bc();
        check_bc(bc, &vec!["most"]);
        assert_eq!(b.version(), Ok(0));
    }

    #[test]
    fn test_scm() {
        let args = vec!["rub".to_string(),
                        "node".to_string(),
                        "scm".to_string()];
        let mut cr = NodeRub::new();
        check_cr(&cr);
        let b = Buildable::new(&mut cr, &args);
        let bc = b.get_bc();
        check_bc(bc, &vec!["scm"]);
        assert_eq!(b.version(), Ok(0));
    }

    #[test]
    fn test_all() {
        let args = vec!["rub".to_string(),
                        "node".to_string(),
                        "all".to_string()];
        let mut cr = NodeRub::new();
        check_cr(&cr);
        let b = Buildable::new(&mut cr, &args);
        let bc = b.get_bc();
        check_bc(bc, &vec!["all"]);
        assert_eq!(b.version(), Ok(0));
    }
}
